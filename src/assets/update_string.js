function get_bash_cmd(appName) {
// assumes app is in desktop
//validator
if(!(appName.indexOf("..")==-1 && appName.length>0 && appName.indexOf(".app")!=-1)) throw Error("get_bash_cmd(): invalid app name")

const newAppName = appName.slice(0,-4) + " old" + ".app"
const bash_cmd = `PATH=$PATH:/usr/local/bin/
cd && cd desktop
if [ -e recipes.zip ]
then
    echo "zip already exists"
    exit 1
fi
wget https://github.com/joshuafriedman/Recipes/releases/latest/download/recipes.zip
mv "${appName}" "${newAppName}"
unzip recipes.zip
rm recipes.zip `;
return bash_cmd;
}

export{
  get_bash_cmd
};
